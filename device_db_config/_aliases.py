# List of aliases for the hardware devices present in _device_db.py.
# These should be friendly names that describe the purpose of a connection.
# These are just examples, add your own!
aliases = {
    "suservo_aom_doublepass_461_injection": "suservo1_ch3",
    "suservo_aom_singlepass_461_spectroscopy": "suservo1_ch7",
    "suservo_aom_singlepass_461_pushbeam": "suservo1_ch2",
    "suservo_aom_singlepass_461_2dmot_a": "suservo1_ch0",
    "suservo_aom_singlepass_461_2dmot_b": "suservo1_ch1",
}
